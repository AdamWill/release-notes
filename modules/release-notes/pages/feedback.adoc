
include::partial$entities.adoc[]

[[sect-Release_Notes-Feedback]]
= Feedback

Thank you for taking the time to provide your comments, suggestions, and bug reports to the Fedora community; this helps improve the state of Fedora, Linux, and free software worldwide.

== Providing Feedback on Fedora Software

To provide feedback on Fedora software or other system elements, please refer to link:++https://docs.fedoraproject.org/en-US/quick-docs/bugzilla-file-a-bug/++[Bugs And Feature Requests]. A list of commonly reported bugs and known issues for this release is available from link:{COMMONBUGS_URL}[Common Bugs] on the forums.

== Providing Feedback on Release Notes

If you feel these release notes could be improved in any way, you can provide your feedback directly to the beat writers. There are several ways to provide feedback, in order of preference:

* Open an issue at link:{BZURL}[] - *This link is ONLY for feedback on the release notes themselves.* Refer to the admonition above for details.

* E-mail the Release-Note mailing list at link:mailto:relnotes@fedoraproject.org[relnotes@fedoraproject.org].
